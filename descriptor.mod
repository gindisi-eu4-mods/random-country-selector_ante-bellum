name="Random Country Selector for Ante Bellum"
version="1.2.5"
picture="thumbnail.png"
dependencies={
	"Ante Bellum"
	"Random Country Selector"
}
tags={
	"Gameplay"
	"Utilities"
	"Events"
	"Map"
}
supported_version="v1.37.*.*"
remote_file_id="2922047470"