Random Country Selector for Ante Bellum v1.2.5 for Europa Universalis IV v1.37 and Ante Bellum v1.9

FEATURES:
Makes my Random Country Selector mod's mission settings account for the missions added/removed by Ante Bellum.

The mod allows you to randomly change to another country after a certain length of time, and includes lots of settings to customize which countries can be selected (countries with missions, on certain continents, etc.)

REQUIREMENTS:
Mods: Requires Ante Bellum and Random Country Selector
DLC: Does not require any DLC.

COMPATIBILITY:
Multiplayer compatible.
Not ironman compatible.

All updates should be savegame compatible, if any aren't, all previous versions are available to download on GitLab.

LINKS:
Steam Workshop: https://steamcommunity.com/sharedfiles/filedetails/?id=2922047470
Gitlab: https://gitlab.com/gindisi-eu4-mods/random-country-selector_ante-bellum
Discord: https://discord.gg/EmYyp4UDmN

Ante Bellum: https://steamcommunity.com/sharedfiles/filedetails/?id=1908400374
Random Country Selector: https://steamcommunity.com/sharedfiles/filedetails/?id=2878725178


If you have any suggestions for new mod features, or have found any bugs in the mod, let me know in the Steam comments or in my Discord server.