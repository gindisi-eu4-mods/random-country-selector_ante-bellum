country_event = { #Mission Settings - Event 5
	id = rcs_events.5
	title = "rcs_events.5.t"
	desc = "rcs_events.5.d"
	picture = BIG_BOOK_eventPicture

	is_triggered_only = yes

	option = { #Large unique mission trees only
		name = "rcs_events.5.1"
		custom_tooltip = rcs_large_unique_mission_tree_tt
		hidden_effect = {
			PIR = {
				set_variable = {
					which = rcs_var_missions
					value = 3
				}
			}
			country_event = { id = rcs_events.3 } #Settings event
		}
	}
	#option = { #Unique mission trees only
	#	name = "rcs_events.5.2"
	#	custom_tooltip = rcs_unique_mission_tree_tt
	#	hidden_effect = {
	#		PIR = {
	#			set_variable = {
	#				which = rcs_var_missions
	#				value = 2
	#			}
	#		}
	#		country_event = { id = rcs_events.3 } #Settings event
	#	}
	#}
	option = { #Regional or unique mission trees
		name = "rcs_events.5.3"
		custom_tooltip = rcs_regional_mission_tree_tt
		hidden_effect = {
			PIR = {
				set_variable = {
					which = rcs_var_missions
					value = 1
				}
			}
			country_event = { id = rcs_events.3 } #Settings event
		}
	}
	option = { #Allow any country regardless of missions
		name = "rcs_events.5.4"
		hidden_effect = {
			PIR = {
				set_variable = {
					which = rcs_var_missions
					value = 0
				}
			}
			country_event = { id = rcs_events.3 } #Settings event
		}
	}
	option = { #Back
		name = "rcs_events.5.5"
		hidden_effect = {
			country_event = { id = rcs_events.3 } #Settings event
		}
	}
}